const {Schema, model} = require('mongoose');
const {newLoad} = require('config').get('UberTruck.loadStatus');
const schema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    default: 'Load has been created',
  },
  assigned_to: {
    type: String,
    default: 'NA',
  },
  status: {
    type: String,
    default: newLoad,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
  },
  logs: {
    type: Array,
    default: [
      {
        message: 'Init message',
        time: new Date(Date.now()),
      },
    ],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
})
;
module.exports = model('Load', schema);
;

