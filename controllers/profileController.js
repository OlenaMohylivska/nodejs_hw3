/* eslint-disable camelcase */
const {successRes, failRequest} = require('../helpers');
const {deleteAllLoads} = require('../models/dao/loadDAO');
const {userFind, userDelete} = require('../models/dao/userDAO');
const {saltRounds} = require('config').get('UberTruck');
const bCrypt = require('bcryptjs');

const getUserInfo = async (req, res) => {
  const {_id, email, created_date} = await userFind(req.userInfo.email);

  return successRes({user: {
    _id,
    email,
    created_date,
  }}, res);
};
const deleteProfile = async (req, res) => {
  const {email, _id} = req.userInfo;
  await userDelete(email);
  await deleteAllLoads({created_by: _id});
  successRes({message: 'Profile was deleted'}, res);
};

const changePassword = async (req, res) => {
  const {email} = req.userInfo;
  const {oldPassword, newPassword} = req.body;
  const user = await userFind(email);
  const isValidPassword = await bCrypt.compare(oldPassword, user.password);

  if (!isValidPassword) {
    return failRequest('Invalid old password', res);
  }

  user.password = await bCrypt.hash(newPassword, saltRounds);
  await user.save();
  successRes({message: 'Password has been changed'}, res);
};

module.exports = {
  getUserInfo,
  deleteProfile,
  changePassword,
}
;
