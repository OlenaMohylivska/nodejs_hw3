const {
  messageToLoadLog,
  successRes,
  failRequest } = require('../helpers');
const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');

const [enRoutePick, pickUp, toDelivery, arrivedDeliv] = require('config').get('UberTruck.loadState');
const { assigned, shipped } = require('config').get('UberTruck.loadStatus');
const { inService, onLoad } = require('config').get('UberTruck.trucksStatuses');

const getLoadInfForDriver = async (req, res) => {
  const { _id } = req.userInfo;
  const activeLoad = await Load.find({
    assigned_to: { $ne: 'NA' },
  });
  const load = activeLoad.find(item => item.assigned_to === _id) || {};

  if (!load) {
    return successRes({ message: "no Active load", load }, res);
  }

  return successRes({ load }, res);
};

const updateState = async (req, res) => {
  const { _id } = req.userInfo;
  const activeLoad = await Load.find({
    assigned_to: { $ne: 'NA' }
  });
  const load = activeLoad.find(item => item.assigned_to === _id);
  
  if(!load || load.status === shipped) {
    return successRes({message: 'Hello'}, res);
  }

  switch (load.state) {
    case enRoutePick:
      await Load.updateOne({
        assigned_to: _id
      },
        {
          state: pickUp,
        });
      await Truck.updateOne({ assigned_to: _id, }, { status: onLoad });

      return successRes({
        message: 'State updated',
      }, res);

    case pickUp:
      await Load.updateOne({
        assigned_to: _id
      },
        {
          state: toDelivery,
        });

      return successRes({
        message: 'State updated',
      }, res);
    case toDelivery:
      await Load.updateOne({
        assigned_to: _id
      },
        {
          state: arrivedDeliv,
          status: shipped,
        });
      await Truck.updateOne({ assigned_to: _id, }, { status: inService });

      return successRes({
        message: 'State updated',
      }, res);
    default:
      return successRes({message:'nothing to iterate'}, res);
  }
};

module.exports = {
  updateState,
  getLoadInfForDriver,
}
  ;
