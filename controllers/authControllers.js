const bCrypt = require('bcryptjs');
const User = require('../models/userModel');
const saltRounds = require('config').get('UberTruck.saltRounds');
const secret = require('config').get('UberTruck.secret');
const {successRes, failRequest} = require('../helpers');
const {existErrorCode} = require('../constants');
const {userFind} = require('../models/dao/userDAO');
const JWT = require('jsonwebtoken');
const newPasswordSender = require('../helpers/newPasswordSender');
const generator = require('generate-password');


const userRegistration = async (req, res) => {
  const {email, password, role} = req.body;

  try {
    const user = new User({
      email,
      role,
      password: await bCrypt.hash(password, saltRounds),
    });

    await user.save();
    return successRes({message: 'User has been saved'}, res);
  } catch (err) {
    if (err.code === existErrorCode) {
      return failRequest('User already exist', res);
    }
  }
};

const userLogin = async (req, res) => {
  const {email, password} = req.body;
  const user = await userFind(email);
  const {_id, role} = user;

  if (!await bCrypt.compare(password, user.password)) {
    return failRequest('Wrong password!', res);
  }

  // eslint-disable-next-line camelcase
  const jwt_token = JWT.sign({
    _id,
    email,
    role,
  }, secret);

  return successRes({
    message: 'Success',
    jwt_token,
  }, res);
};

const sendNewPassword = async (req, res) => {
  const {email} = req.body;
  const user = await userFind(email);
  const newPassword = generator.generate({
    length: 10,
    numbers: true,
  });
  const text = `Your new password is ${newPassword}`;

  user.password = await bCrypt.hash(newPassword, saltRounds);
  await newPasswordSender(user.email, text);
  await user.save();
  successRes({
    message: 'New password sent to your email address',
  }, res);
};

module.exports = {
  userRegistration,
  userLogin,
  sendNewPassword,
}
;
