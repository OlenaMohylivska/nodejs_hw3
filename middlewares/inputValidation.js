/* eslint-disable new-cap */
const Joi = require('joi');
const {passwordRegExp} = require('../constants');
const {failRequest} = require('../helpers');

module.exports.registration = async function(req, res, next) {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}})
        .required(),
    password: Joi.string()
        .pattern(new RegExp(passwordRegExp))
        .required(),
    role: Joi.string()
        .valid('SHIPPER', 'DRIVER')
        .required(),
  });
  await schema.validateAsync(req.body)
      .then(() => next())
      .catch((err) => {
        return failRequest(err.message, res);
      });
};

module.exports.email = async function(req, res, next) {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2})
        .required(),
  });
  await schema.validateAsync(req.body)
      .then(() => next())
      .catch((err) => {
        return failRequest(err.message, res);
      });
};

module.exports.login = async function(req, res, next) {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2})
        .required(),
    password: Joi.string()
        .pattern(new RegExp(passwordRegExp))
        .required(),
  });
  await schema.validateAsync(req.body)
      .then(() => next())
      .catch((err) => {
        return failRequest(err.message, res);
      });
};
module.exports.truckType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .required()
        .case('upper'),
  });
  await schema.validateAsync(req.body)
      .then(() => next())
      .catch((err) => {
        return failRequest(err.message, res);
      });
};

module.exports.loadInfoValidation = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .integer()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object()
        .keys({
          width: Joi.number().required(),
          length: Joi.number().required(),
          height: Joi.number().required(),
        })
        .required(),
  });
  await schema.validateAsync(req.body)
      .then(() => next())
      .catch((err) => {
        return failRequest(err.message, res);
      });
};
