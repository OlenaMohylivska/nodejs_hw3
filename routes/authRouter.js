/* eslint-disable new-cap */
const {Router} = require('express');
const route = Router();
const {
  userRegistration,
  userLogin,
  sendNewPassword,
} = require('../controllers/authControllers');
const {asyncErrHandler} = require('../helpers');
const {login, registration, email} = require('../middlewares/inputValidation');
const {user} = require('../middlewares/doesDocumentExist');

route.post('/register',
    asyncErrHandler(registration),
    asyncErrHandler(userRegistration));

route.post('/login',
    asyncErrHandler(login),
    asyncErrHandler(user),
    asyncErrHandler(userLogin));

route.post('/forgot_password',
    asyncErrHandler(email),
    asyncErrHandler(sendNewPassword));

module.exports = route;
