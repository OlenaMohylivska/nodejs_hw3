const {Router} = require('express');
// eslint-disable-next-line new-cap
const route = Router();
const {
  getUserInfo,
  deleteProfile,
  changePassword,
} = require('../controllers/profileController');
const {asyncErrHandler} = require('../helpers');
const {user} = require('../middlewares/doesDocumentExist');
const jwtVer = require('../middlewares/jwtVer');
const {doesUserShipper} = require('../middlewares/roleChecker');

route.get('/me',
    asyncErrHandler(jwtVer),
    asyncErrHandler(user),
    asyncErrHandler(getUserInfo));

route.delete('/me',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserShipper),
    asyncErrHandler(user),
    asyncErrHandler(deleteProfile));

route.patch('/me/password',
    asyncErrHandler(jwtVer),
    asyncErrHandler(user),
    asyncErrHandler(changePassword));

module.exports = route;
