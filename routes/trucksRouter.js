const {Router} = require('express');
// eslint-disable-next-line new-cap
const route = Router();
const {
  addTruckForUSer,
  getAllUsersTrucks} = require('../controllers/trucks/trucksController');
const {asyncErrHandler} = require('../helpers');
const {user} = require('../middlewares/doesDocumentExist');
const {truckType} = require('../middlewares/inputValidation');
const jwtVer = require('../middlewares/jwtVer');
const {doesUserDriver} = require('../middlewares/roleChecker');

route.post('/',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(truckType),
    asyncErrHandler(user),
    asyncErrHandler(addTruckForUSer))
;

route.get('/',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesUserDriver),
    asyncErrHandler(user),
    asyncErrHandler(getAllUsersTrucks))
;

module.exports = route;
