const mongoose = require('mongoose');
const {Types: {ObjectId}} = mongoose;
const config = require('config');
const {httpResponseStatus: {
  serverError,
  notFound,
  success,
  badRequest,
}} = require('../constants');
const {findTruckByCriteria} = require('../models/dao/truckDAO');
const {findUserLoadCriteria} = require('../models/dao/loadDAO');
const {
  SPRINTER,
  SMALL_STRAIGHT,
  LARGE_STRAIGHT} = require('config').get('UberTruck.truckTypes');
const {inService} = require('config').get('UberTruck.trucksStatuses');

const asyncErrHandler = (fn) => (req, res, next) => {
  return Promise
      .resolve(fn(req, res, next))
      .catch(next);
};

const mainErrHandler = (error, req, res, next) => {
  res.status(error.status || serverError);
  res.json({
    status: error.status,
    message: error.message,
    stack: error.stack,
  });
};

const badReq = (req, res) => {
  return res.status(notFound).json({message: '404! Not found'});
};

const dbConnect = async (mongoose) => {
  const {dbName, dbUser, dbPassword} = config.get('UberTruck.dbConfig');
  const URI = `mongodb+srv://${dbUser}:${dbPassword}@cluster1.nwslh.mongodb.net/${dbName}?retryWrites=true&w=majority`;

  try {
    await mongoose.connect(URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log('db has been connected!');
  } catch (err) {
    console.log(err);
  }
};

const validateObjectId = (id) => ObjectId.isValid(id) &&
   (new ObjectId(id)).toString() === id;


const internalServerError = (message, res) => {
  return res.status(serverError).json({message: message});
};

const failRequest = (message, res) => {
  return res.status(badRequest).json({message: message});
};

const successRes = (obj, res) => {
  return res.status(success).json(obj);
};

const capacityDefiner = (typeTruck) => {
  const type = typeTruck.toUpperCase() || '';

  switch (type) {
    case 'SPRINTER':
      return SPRINTER;
    case 'LARGE STRAIGHT':
      return LARGE_STRAIGHT;
    case 'SMALL STRAIGHT':
      return SMALL_STRAIGHT;
    default: return {volume: undefined, payload: undefined};
  }
};

const messageToLoadLog = (message) => {
  return {
    message,
    created_date: new Date(Date.now()),
  };
};
const activeLoadFinder = async (_id, res) => {
  const [userTruck] = await findTruckByCriteria({
    assigned_to: _id,
  });

  if (!userTruck) {
    return failRequest('No active loads', res);
  }

  const activeLoad = await findUserLoadCriteria({
    assigned_to: userTruck['_id'],
  });
  activeLoad.logs.push(messageToLoadLog('Driver req info about load.'));
  return activeLoad;
};
module.exports = {
  asyncErrHandler,
  mainErrHandler,
  badReq,
  dbConnect,
  validateObjectId,
  failRequest,
  internalServerError,
  successRes,
  capacityDefiner,
  messageToLoadLog,
  activeLoadFinder,
}
;
