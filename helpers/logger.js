const MONGOOSE_MORGAN = require('mongoose-morgan');
const CONFIG = require('config');
const {dbUser, dbPassword, dbName} = CONFIG.get('UberTruck.dbConfig');

const URI = `mongodb+srv://${dbUser}:${dbPassword}@cluster1.nwslh.mongodb.net/${dbName}?retryWrites=true&w=majority`;
const MONGO_MORGAN = MONGOOSE_MORGAN(  
      {   
       collection: 'request-logs',    
       connectionString: URI  
      },

      {},
   'tiny'
); 

module.exports = MONGO_MORGAN;