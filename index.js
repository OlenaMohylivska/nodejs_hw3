const app = require('./app');
const {otherPort} = require('config').get('UberTruck.dbConfig');

app.listen(process.env.PORT || otherPort,
    () => console.log(`Server has been started on port ${process.env.PORT}`));
